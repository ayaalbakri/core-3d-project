﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using blob.Models;
using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Newtonsoft.Json.Linq;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace blob.Controllers
{
    [Route("api/[controller]")]
    public class Neo : Controller
    {
        public Neo()
        {
            AllAntsAtMap = new List<NodeNew>();

        }
        Neo4jClient.GraphClient client = new GraphClient(new Uri("http://40.114.110.145:7474/db/data"), "neo4j", "Database123!");

        public List<NodeNew> AllAntsAtMap { get; set; }

        // GET: /<controller>/
        [HttpGet("Node")]
        public JsonResult AnatomyObj()
        {
            client.Connect();

            IEnumerable<NodeNew> results = client.Cypher
                .Match("(n)")
                .Return<NodeNew>("n")
                .Results;
            foreach (NodeNew node in results)
            {
                if (node.isdelete == false)
                {

                    AllAntsAtMap.Add(node);
                }
            }
            return Json(AllAntsAtMap);
        }
        //*********get Details About specific NODE*********
        [HttpPost("AddNode")]
        public JsonResult AddNode([FromBody] NodeNew newNode)
        {
            client.Connect();
			var uniq = client.Cypher
		   .Match("(n:Node{name:" + "'" + newNode.name + "'" + "} )")
		   .Return<NodeNew>("n")
		   .Results;
			if (uniq.Count() ==  0)
			{
				client.Cypher
			 .Merge("(n:Node {name:" + "'" + newNode.name + "'" + ",description:" + "'" + newNode.description + "'" + ",type:" + "'" + newNode.type + "'" + ",FID:" + "'" + Guid.NewGuid() + "'" + ",isdelete:false })")
			 .ExecuteWithoutResults();
			}
            return Json("n");
        }



        //*********get Details About specific NODE*********
        [HttpPost("Details")]
        public JsonResult AnatomyObjDetails([FromBody] JObject detail)
        {
            var x = detail["name"].ToString();
            client.Connect();
            IEnumerable<NodeNew> results = client.Cypher
                .Match("(n:Node{name :" + "'" + x + "'" + "})")
                .Return<NodeNew>("n")
                .Results;
            return Json("hiiiii");
        }

        //Make relation between objects
        [HttpPost("relation")]
        public JsonResult Relation([FromBody] JObject x)
        {
            var vend = x["venderName"].ToObject<string>();
            var anat = x["anatomyName"].ToObject<string>();
            var relation = x["relation"].ToObject<string>();
            client.Connect();
            IEnumerable<relation> createRelation = client.Cypher
                .Match("(a {name:" + "'" + vend + "'" + "}),(b {name:" + "'" + anat + "'" + "})")
                .CreateUnique("(a) -[m:" + relation + " {Anatomyname:" + "'" + anat + "'" + "," + "Vendorname:" + "'" + vend + "'" + "," + "type:" + "'" + relation + "'" + "}]- (b)")
                .Return<relation>("m")
                .Results;
            return Json(createRelation);
        }

        //Get All relation of specific Vendor
        [HttpPost("relationVender")]
        public JsonResult RelationVender([FromBody] JObject x)
        {
            var vend = x["venderName"].ToObject<string>();
            client.Connect();
            IEnumerable<relation> relation = client.Cypher
               .Match("(a:Vendor{name:" + "'" + vend + "'" + "})-[m] - (b)")
                .Return<relation>("m")
               .Results;
            return Json(relation);
        }
        //Get All relation for specific Anatomy
        [HttpPost("relationAnatomy")]
        public JsonResult relationAnatomy([FromBody] JObject x)
        {
            var Anatomy = x["anatomyName"].ToObject<string>();
            Console.WriteLine("saSAsaS" + Anatomy);
            client.Connect();
            IEnumerable<relation> relation = client.Cypher
               .Match("(a:Anatomy{name:" + "'" + Anatomy + "'" + "})-[m] - (b)")
                .Return<relation>("m")
               .Results;
            return Json(relation);
        }


        //Get ALL Relation
        [HttpGet("allRelation")]
        public JsonResult allRelation()
        {
            client.Connect();
            IEnumerable<relation> relation = client.Cypher
               .Match("(a)-[m] - (b)")
               .Return<relation>("m")
               .Results;
            return Json(relation);
        }

        //Delete Node
        [HttpPost("deleteAnatomy")]
        public JsonResult deleteAnatomy([FromBody] NodeNew deleteObj)
        {
            deleteObj.isdelete = true;


            client.Connect();
			//client.Connect();
			IEnumerable<NodeNew> results = client.Cypher
			   .Match("(n:Node{FID:" + "'" + deleteObj.FID + "'" + "})")
			  .Set("n.isdelete= true")
				.Return<NodeNew>("n")
				.Results;
			return Json(results);
		}
        /// <summary>
        /// Edit Node
        /// </summary>
        /// <param name="FID"></param>
        /// <param name="Ant"></param>
        /// <returns></returns> 
        [HttpPost("updateNode/{FID}")]
        public JsonResult updateAnatomy(string FID, [FromBody] NodeNew Ant)
        {
			// at { name: 'Andres' }),(pn { name: 'Peter' }
			//MATCH(charlie: Person { name: 'Charlie Sheen' }),(wallStreet: Movie { title: 'Wall Street' })
			client.Connect();
			var results = client.Cypher
			  .Match("(n{FID:" + "'"+  Ant.FID +"'" +  "})")
			 .Set("n = {Ant}")
				.WithParams(new
				{
					name = Ant.name,
					description = Ant.description,
					isdelete = Ant.isdelete,
					Ant
				})
			   .Return<NodeNew>("n")
			   .Results;
			return Json(results);

		}
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

