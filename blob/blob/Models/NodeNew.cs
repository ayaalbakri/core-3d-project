﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blob.Models
{
    public class NodeNew
    {
        public string name { get; set; }

		public string FID { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public bool isdelete { get; set; }
    }
}
