﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using blob.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Neo4jClient;
using Newtonsoft.Json.Linq;



namespace blob.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAnyOrigin")]
    public class Neo4jController : Controller
    {

        //public HomeController( )
        //{
        public Neo4jController()
        {
            AllAntsAtMap = new List<NodeNew>();

        }
        Neo4jClient.GraphClient client = new GraphClient(new Uri("http://40.114.110.145:7474/db/data"), "neo4j", "Database123!");

        public List<NodeNew> AllAntsAtMap { get; set; }


        //*************"GET ALL NODE"**********************
        [HttpGet("Node")]
        public JsonResult AnatomyObj()
        {
            client.Connect();

            IEnumerable<NodeNew> results = client.Cypher
                .Match("(n)")
                .Return<NodeNew>("n")
                .Results;
            foreach (NodeNew node in results)
            {
                if (node.isdelete == false)
                {

                    AllAntsAtMap.Add(node);
                }
            }
            return Json(AllAntsAtMap);
        }


        //*********get Details About specific NODE*********
        [HttpPost("Details")]
        public JsonResult AnatomyObjDetails([FromBody] JObject name)
        {
            var x = name["name"];
            client.Connect();
            IEnumerable<NodeNew> results = client.Cypher
                .Match("(n:Node{name :" + "'" + x + "'" + "})")
                .Return<NodeNew>("n")
                .Results;
            return Json(results);
        }

        //Make relation between objects
        [HttpPost("relation")]
        public JsonResult Relation([FromBody] JObject x)
        {
            var vend = x["venderName"].ToObject<string>();
            var anat = x["anatomyName"].ToObject<string>();
            var relation = x["relation"].ToObject<string>();
            client.Connect();
            IEnumerable<relation> createRelation = client.Cypher
                .Match("(a:{name:" + "'" + vend + "'" + "}),(b:{name:" + "'" + anat + "'" + "})")
                .CreateUnique("(a) -[m:" + relation + " {Anatomyname:" + "'" + anat + "'" + "," + "Vendorname:" + "'" + vend + "'" + "," + "type:" + "'" + relation + "'" + "}]- (b)")
                .Return<relation>("m")
                .Results;
            return Json(createRelation);
        }

        //Get All relation of specific Vendor
        [HttpPost("relationVender")]
        public JsonResult RelationVender([FromBody] JObject x)
        {
            var vend = x["venderName"].ToObject<string>();
            client.Connect();
            IEnumerable<relation> relation = client.Cypher
               .Match("(a:Vendor{name:" + "'" + vend + "'" + "})-[m] - (b)")
                .Return<relation>("m")
               .Results;
            return Json(relation);
        }
        //Get All relation for specific Anatomy
        [HttpPost("relationAnatomy")]
        public JsonResult relationAnatomy([FromBody] JObject x)
        {
            var Anatomy = x["anatomyName"].ToObject<string>();
            Console.WriteLine("saSAsaS" + Anatomy);
            client.Connect();
            IEnumerable<relation> relation = client.Cypher
               .Match("(a:Anatomy{name:" + "'" + Anatomy + "'" + "})-[m] - (b)")
                .Return<relation>("m")
               .Results;
            return Json(relation);
        }


        //Get ALL Relation
        [HttpGet("allRelation")]
        public JsonResult allRelation()
        {
            client.Connect();
            IEnumerable<relation> relation = client.Cypher
               .Match("(a)-[m] - (b)")
               .Return<relation>("m")
               .Results;
            return Json(relation);
        }

        //Delete Node
        [HttpPost("deleteAnatomy")]
        public JsonResult deleteAnatomy([FromBody] NodeNew deleteObj)
        {
            deleteObj.isdelete = true;


            client.Connect();
            var results = client.Cypher
               .Match("(n{FID:" + deleteObj.FID + "})")
              .Set("n = {deleteObj}")
                 .WithParams(new
                 {
                     name = deleteObj.name,
                     description = deleteObj.description,
                     FID = deleteObj.description,
                     isdelete = deleteObj.isdelete,
                     deleteObj
                 })
                .Return<NodeNew>("n")
                .Results;
            return Json(results);
        }
        /// <summary>
        /// Edit Node
        /// </summary>
        /// <param name="FID"></param>
        /// <param name="Ant"></param>
        /// <returns></returns>
        [HttpPost("updateNode/{FID}")]
        public JsonResult updateAnatomy(string FID, [FromBody] NodeNew Ant)
        {
            // at { name: 'Andres' }),(pn { name: 'Peter' }
            //MATCH(charlie: Person { name: 'Charlie Sheen' }),(wallStreet: Movie { title: 'Wall Street' })
            client.Connect();
            Ant.FID = FID;
            var results = client.Cypher
              .Match("(n{FID:" + FID + "})")
             .Set("n = {Ant}")
                .WithParams(new
                {
                    name = Ant.name,
                    description = Ant.description,
                    FID = Ant.FID,
                    isdelete = Ant.isdelete,
                    Ant
                })
               .Return<NodeNew>("n")
               .Results;
            return Json(results);

        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
