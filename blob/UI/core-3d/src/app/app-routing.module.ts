import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ObjectComponent } from './object/object.component';
import { ListComponent } from './list/list.component';
const routes: Routes = [
  { path: '', component: ObjectComponent },
  { path: 'list', component: ListComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }