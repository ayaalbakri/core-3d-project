﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace blob.Models
{
    public class Node
    {
        public string name { get; set; }
        public int FID { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public bool isdelete { get; set; }
    }
}
